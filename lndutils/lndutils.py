"""
   Copyright 2019 BT
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
   limitations under the License.

"""

import base64, codecs, json, requests

def getinfo(hostport='localhost:8181', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    return request(hostport=hostport, request='getinfo', macaroon_path=macaroon_path, cert_path=cert_path).json()

def channels(hostport='localhost:8181', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    return request(hostport=hostport, request='channels', macaroon_path=macaroon_path, cert_path=cert_path).json()

def invoices(hostport='localhost:8181', request_type='fetch', data='', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    if request_type == "fetch":
        return request(hostport=hostport, request='invoices', macaroon_path=macaroon_path, cert_path=cert_path).json()
    elif request_type == "create":
        if data != '':
            return request(hostport=hostport, request='invoices', request_type='POST', request_data=data, macaroon_path=macaroon_path, cert_path=cert_path).json()
        else:
            return json.dumps({"error": "Must specify 'data' attribute"}, ensure_ascii=False)
    else:
        return json.dumps({"error": "Must specify request_type - fetch or create. Create must have data conforming to LND invoice creation spec as a dictionary"}, ensure_ascii=False)

def balance(hostport='localhost:8181', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    error = False
    try:
        blockchain = request(hostport=hostport, request='balance/blockchain', macaroon_path=macaroon_path, cert_path=cert_path)
    except:
        error = True
    else:
        try:
            channels = request(hostport=hostport, request='balance/channels', macaroon_path=macaroon_path, cert_path=cert_path)
        except:
            error = True
        else:
            try:
                blockchain_dict = blockchain.json()
                channels_dict = channels.json()
                chan_balance = int(channels_dict['balance'])
                blockchain_total = blockchain_dict['total_balance']
                blockchain_confirmed = blockchain_dict['confirmed_balance']
                avail_balance = int(chan_balance) + int(blockchain_confirmed)
            except:
                error = True
            else:
                data =  {"channel": chan_balance, "onchain": {"avail": int(blockchain_confirmed), "total": int(blockchain_total)}, "total_available_balance": avail_balance}
                return json.dumps(data, ensure_ascii=False)

def emptywallet(hostport='localhost:8181', address='', data='', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    if address != '':
        return json.dumps({"message": "PLACEHOLDER: This would empty LND's wallet into address " + address}, ensure_ascii=False)
    else:
        return json.dumps({"error": "Please specify address"}, ensure_ascii=False)

def request(hostport='localhost:8181', request='getinfo', request_type='GET', request_data='', macaroon_path='./readonly.macaroon', cert_path='./tls.cert'):
    url='https://'+hostport+'/v1/' + request
    macaroon = codecs.encode(open(macaroon_path, 'rb').read(), 'hex')
    headers = {'Grpc-Metadata-macaroon': macaroon}
    try:
        if request_type == 'GET':
            r = requests.get(url, headers=headers, verify=cert_path)
        else:
            if request_data != '':
                r = requests.post(url, headers=headers, verify=cert_path, data=json.dumps(request_data))
            else:
                return json.dumps({"error": "Please specify some request data"}, ensure_ascii=False)
    except:
        return json.dumps({"error": "An error has occured"}, ensure_ascii=False)
    else:
        return r

if __name__ == "__main__":
    print("this file should not be run directly")
