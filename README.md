# LND Utils

## Installing from source

```
git clone git@gitlab.com:nolim1t/lndtools.git
cp -fr ./lndutils/lndtools /path/to/source

```

## Installing from PIP

coming soon

## Example code

```
from lndutils import lndutils


# to specify another hostport, add hostport=''

# get info (default: localhost:8181)
print(lndutils.getinfo())

# get info (specify a host / port)
print(lndutils.getinfo(hostport='host:port'))

# get balance (default: localhost:8181)
print(lndutils.balance())
```
